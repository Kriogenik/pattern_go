package builder

type Builder interface {
	makeHeader(str string)
	makeContent(str string)
	makeFooter(str string)
}

type Director struct {
	builder Builder
}

type Product struct {
	Header  string
	Content string
	Footer  string
}

func (self *Director) Construct() {
	self.builder.makeHeader("Header")
	self.builder.makeContent("Content")
	self.builder.makeFooter("Footer")
}

type ConcreteBuilder struct {
	product *Product
}

func (self *ConcreteBuilder) makeHeader(str string) {
	self.product.Header = "<header>" + str + "</header>\n"
}

func (self *ConcreteBuilder) makeContent(str string) {
	self.product.Content = "<article>" + str + "</article>\n"
}

func (self *ConcreteBuilder) makeFooter(str string) {
	self.product.Footer = "<footer>" + str + "</footer>\n"
}

func (self *Product) Show() string {
	var result string
	result += self.Header
	result += self.Content
	result += self.Footer
	return result
}
